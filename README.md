# How to start HelloJPro #


## Web Browser ##

### Start jpro in foreground (development mode) ###

```
gradle runBrowser
```


### Start jpro in background (server mode) ###

```
gradle restartServer
```


### Open jpro app in Web Browser ###
```
http://localhost:9000/index.html
```

### Show all jpro apps in Browser ####
```
http://localhost:9000/test/default
```

### Open jpro app in fullscreen ####
```
http://localhost:9000/test/fullscreen/[app-name]
```

## Licence Key ##
To use jpro please specify your jpro license in jpro.conf file located at [project-dir]/src/main/resources/


```
jpro.license {
  username = ""
  password = ""
  licensenumber = ""
}
```

